package com.twu.biblioteca;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class MovieTest {

    @Test
    public void equalsShouldReturnIfTwoInstancesAreEqual()
    {
        Movie movie = new Movie("santhosham",2002,"K.Dasarath",10);
        assertTrue(movie.equals(new Movie("santhosham",2002,"K.Dasarath",10)));
    }

    @Test
    public void toStringShouldReturnMovieDetails()
    {
        Movie movie = new Movie("santhosham",2002,"K.Dasarath",10);
        String details = String.format("%-25s%-25d%-25s%-25f\n\n","santhosham",2002,"K.Dasarath",10.0);
        assertTrue(movie.toString().equals(details));
    }

    @Test
    public void searchNameShouldReturnFalseIfIGiveNameOfMovieWhichIsNotInMovieList() {
        Movie movie = new Movie("santhosham",2002,"K.Dasarath",10);
        assertFalse(movie.searchMovie("Pirates of the Caribbean"));
    }

    @Test
    public void searchNameShouldReturnTrueIfIGiveNameOfMovieWhichIsInMovieList() {
        Movie movie = new Movie("santhosham",2002,"K.Dasarath",10);
        assertTrue(movie.searchMovie("santhosham"));
    }
}
