package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class LibraryTest {

    private Library library;
    private MovieList movieList;
    private BookList bookList;

    @Before
    public void setUp() throws FileNotFoundException {
        bookList = mock(BookList.class);
        movieList = mock(MovieList.class);
    }

    @Test
    public void displayBookListShouldReturnBookDetails() {
        when(bookList.isEmpty()).thenReturn(false);
        Book book = mock(Book.class);
        when(bookList.size()).thenReturn(1);
        when(bookList.contains(book)).thenReturn(true);
        verify(bookList, never()).isEmpty();
        library = new Library(bookList, movieList);
        bookList.add(book);
        assertEquals(1, bookList.size());
        assertTrue(bookList.contains(book));
    }

    @Test
    public void displayMovieListShouldReturnMovieDetails() {
        when(movieList.isEmpty()).thenReturn(false);
        Movie movie = mock(Movie.class);
        when(movieList.size()).thenReturn(1);
        when(movieList.contains(movie)).thenReturn(true);
        verify(movieList, never()).isEmpty();
        library = new Library(bookList, movieList);
        movieList.add(movie);
        assertEquals(1, movieList.size());
        assertTrue(movieList.contains(movie));
    }

    @Test
    public void welcomeMessageShouldReturnWelcomeMessage() {
        library = new Library(bookList, movieList);
        assertEquals("Hello, Welcome to the Library!!!", library.welcomeMessage());
    }

    @Test
    public void shouldReturnSuccessfulCheckoutMessageIfBookIsInBooksList() {
        User user = mock(User.class);
        Book book = mock(Book.class);
        when(bookList.searchBook("Two States")).thenReturn(book);
        when(bookList.remove(book)).thenReturn(true);
        library = new Library(bookList, movieList);
        String newBookName = "Two States";

        assertTrue(library.checkout(newBookName, user).equals(Library.SUCCESSFUL_CHECKOUT));
    }

    @Test
    public void shouldReturnUnSuccessfulCheckoutMessageIfBookIsNotInBooksList() {
        User user = mock(User.class);
        Book book = mock(Book.class);
        when(bookList.searchBook("Two States")).thenReturn(book);
        when(bookList.remove(book)).thenReturn(true);
        library = new Library(bookList, movieList);

        String newBookName = "Web Technologies";
        assertTrue(library.checkout(newBookName, user).equals(Library.UNSUCCESSFUL_CHECKOUT));
    }

    @Test
    public void shouldReturnUnSuccessfulCheckInMessageIfBookIsNotInBooksList() {
        User user = mock(User.class);
        Book book = mock(Book.class);
        when(bookList.add(book)).thenReturn(false);
        library = new Library(bookList, movieList);
        String bookName = "Web Technologies";
        assertTrue(library.returnBook(bookName, user).equals(Library.UNSUCCESSFUL_CHECKIN));
    }
}
