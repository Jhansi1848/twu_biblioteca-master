package com.twu.biblioteca;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserListTest {
    @Test
    public void addShouldStoreAUserObject() {
        User user = mock(User.class);
        UserList userList = new UserList();
        userList.add(user);
        assertEquals(1, userList.size());
        assertTrue(userList.contains(user));
    }

    @Test
    public void removeShouldRemoveAUserObjectFromList() {
        User user = mock(User.class);
        UserList userList = new UserList();
        userList.add(user);
        assertEquals(1, userList.size());
        userList.remove(user);
        assertEquals(0, userList.size());
        assertFalse(userList.contains(user));
    }

    @Test
    public void getShouldReturnAUserObjectFromList() {
        User user = mock(User.class);
        UserList userList = new UserList();
        userList.add(user);
        assertEquals(1, userList.size());
        assertTrue(userList.get(0).equals(user));
    }

    @Test
    public void searchUserShouldReturnUserIfAUserIsThereInList() throws InvalidDetailsException {
        User user = mock(User.class);
        UserList userList = new UserList();
        userList.add(user);
        when(user.searchUser("123-4567","mypassword")).thenReturn(true);
        assertEquals(1, userList.size());
        assertTrue(userList.searchUser("123-4567","mypassword").equals(user));
    }
}
