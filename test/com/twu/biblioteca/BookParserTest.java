package com.twu.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookParserTest {

    @Test
    public void parseLibraryShouldReturnLibraryObject() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("Data Mining,Witten,1996").thenReturn(null);
        BookParser bookParser = new BookParser(bufferedReader);
        BookList actualBooks = bookParser.parseBook();
        assertEquals(1, actualBooks.size());
    }
}
