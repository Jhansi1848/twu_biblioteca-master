package com.twu.biblioteca;

import org.junit.Test;
import org.omg.CORBA.Object;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;

public class BookListTest {

    @Test
    public void addShouldStoreABookObject()
    {
        Book book = mock(Book.class);
        BookList bookList  =  new BookList();
        bookList.add(book);
        assertEquals(1,bookList.size());
        assertTrue(bookList.contains(book));
    }

    @Test
    public void removeShouldRemoveABookObjectFromList()
    {
        Book book = mock(Book.class);
        BookList bookList  =  new BookList();
        bookList.add(book);
        assertEquals(1,bookList.size());
        bookList.remove(book);
        assertEquals(0,bookList.size());
        assertFalse(bookList.contains(book));
    }

    @Test
    public void getShouldReturnABookObjectFromList()
    {
        Book book = mock(Book.class);
        BookList bookList  =  new BookList();
        bookList.add(book);
        assertEquals(1,bookList.size());
        assertTrue(bookList.get(0).equals(book));
    }

    @Test
    public void searchBookShouldReturnBookIfABookIsThereInList()
    {
        Book book = mock(Book.class);
        BookList bookList  =  new BookList();
        bookList.add(book);
        when(book.searchBook("Data Mining")).thenReturn(true);
        assertEquals(1,bookList.size());
        assertTrue(bookList.searchBook("Data Mining").equals(book));
    }
}
