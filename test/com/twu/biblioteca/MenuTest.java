package com.twu.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class MenuTest {

    @Test
    public void performOperationShouldReturnCheckInMessageIfChooseOperation1() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        User user = mock(User.class);
        when(bufferedReader.readLine()).thenReturn("two states");
        Library library = mock(Library.class);
        when(library.displayBookList()).thenReturn("books");
        Menu menu = new Menu(library,bufferedReader,user);
        //InputHandler inputHandler = new InputHandler(bufferedReader);
        menu.performOperation(1);
        verify(library, times(1)).displayBookList();

    }

    @Test
    public void performOperationShouldReturnCheckOutMessageIfChooseOperation2() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        User user = mock(User.class);
        Library library = mock(Library.class);
        when(bufferedReader.readLine()).thenReturn("Two States");
        Menu menu = new Menu(library,bufferedReader,user);
        menu.performOperation(2);
        verify(library, times(1)).checkout("Two States",user);

    }

    @Test
    public void performOperationShouldReturnCheckInMessageIfChooseOperation3() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        User user = mock(User.class);
        Library library = mock(Library.class);
        when(bufferedReader.readLine()).thenReturn("Two States");
        Menu menu = new Menu(library,bufferedReader,user);
        menu.performOperation(3);
        verify(library, times(1)).returnBook("Two States",user);
    }

    @Test
    public void performOperationShouldReturnLIBRARY_EMPTY_MessageIfBooksNotAvailable() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        User user = mock(User.class);
        Library library = mock(Library.class);
        when(bufferedReader.readLine()).thenReturn("Two States");
        Menu menu = new Menu(library,bufferedReader,user);
        menu.performOperation(3);
        verify(library, times(1)).returnBook("Two States",user);
    }

    @Test
    public void performOperationShouldReturnDefaultMessageIfChooseOperation4() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        User user = mock(User.class);
        Library library = mock(Library.class);
        Menu menu = new Menu(library,bufferedReader,user);
        assertTrue(menu.performOperation(6).equals("Select a valid option!"));
    }
}
