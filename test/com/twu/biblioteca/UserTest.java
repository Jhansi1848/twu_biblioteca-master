package com.twu.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserTest {

    @Test
    public void equalsShouldReturnTrueIfTwoInstancesAreEquals() {
        User user = new User("123-4567","mypassword","account@gmail.com");
        assertTrue(new User("123-4567","mypassword","account@gmail.com").equals(user));
    }

    @Test
    public void toStringShouldReturnUserDetails() {
        User user = new User("123-4567","mypassword","account@gmail.com");
        String details = "User{" + "user_name='" + "123-4567" + '\'' + ", email='" + "account@gmail.com" + '\'' + '}';
        assertTrue(user.toString().equals(details));
    }


    @Test
    public void checkNameShouldReturnTrueIfIGiveUserAuthenticationDetails() {
        User user = new User("123-4567","mypassword","account@gmail.com");
        assertTrue(user.searchUser("123-4567","mypassword"));
    }

    @Test
    public void checkNameShouldReturnFalseIfIGiveWrongUserAuthenticationDetails() {
        User user = new User("123-4567","mypassword","account@gmail.com");
        assertFalse(user.searchUser("123-4567","mypa"));
    }
}
