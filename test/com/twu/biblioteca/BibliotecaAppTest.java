package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class BibliotecaAppTest {

    private Output mockOutput;
    private InputHandler mockInputHandler;
    private Library mockLibrary;
    private BibliotecaApp bibliotecaApp;
    private Menu menu;
    private User user;

    @Before
    public void setUp() {
        mockOutput = mock(Output.class);
        mockInputHandler = mock(InputHandler.class);
        mockLibrary = mock(Library.class);
        bibliotecaApp = new BibliotecaApp();
        menu = mock(Menu.class);
        user = mock(User.class);
    }

    @Test
    public void startShouldCallGetMessageWhenIStartIt() throws IOException {
        when(mockInputHandler.chooseOption()).thenReturn(6);
        System.setOut(mock(PrintStream.class));
        bibliotecaApp.start(mockInputHandler, mockOutput, mockLibrary,user,menu);
        verify(mockLibrary, times(1)).welcomeMessage();
    }

    @Test
    public void startShouldCallListOfBooksWhenIMyOptionIs1() throws IOException {
        when(mockInputHandler.chooseOption()).thenReturn(1).thenReturn(6);
        when(menu.performOperation(1)).thenReturn("");
        System.setOut(mock(PrintStream.class));
        bibliotecaApp.start(mockInputHandler, mockOutput, mockLibrary,user,menu);
    }

    @Test
    public void startShouldCallCheckoutBookWhenIMyOptionIs2() throws IOException {
        when(mockInputHandler.chooseOption()).thenReturn(2).thenReturn(6);
        when(menu.performOperation(2)).thenReturn("Two States");
        System.setOut(mock(PrintStream.class));
        bibliotecaApp.start(mockInputHandler, mockOutput, mockLibrary,user,menu);
        verify(mockOutput, times(1)).show(menu.performOperation(2));
    }

    @Test
    public void startShouldCallReturnBookWhenIChooseOption3() throws IOException {

        when(mockInputHandler.chooseOption()).thenReturn(3).thenReturn(6);
        when(menu.performOperation(3)).thenReturn("Two States");
        when(mockLibrary.returnBook("Two States",user)).thenReturn("");
        System.setOut(mock(PrintStream.class));
        bibliotecaApp.start(mockInputHandler, mockOutput, mockLibrary,user,menu);
        verify(mockOutput, times(1)).show(menu.performOperation(3));
    }

    @Test
    public void startShouldCallCheckoutMovieWhenIChooseOption5() throws IOException {
        when(mockInputHandler.chooseOption()).thenReturn(4).thenReturn(6);
        when(menu.performOperation(4)).thenReturn("movies");
        System.setOut(mock(PrintStream.class));
        when(mockLibrary.displayMovieList()).thenReturn("movies");
        bibliotecaApp.start(mockInputHandler, mockOutput, mockLibrary,user,menu);
        verify(mockOutput, times(1)).show(menu.performOperation(4));
    }

    @Test
    public void startShouldCallListMoviesWhenIChooseOption4() throws IOException {
        when(mockInputHandler.chooseOption()).thenReturn(4).thenReturn(6);
        when(menu.performOperation(4)).thenReturn("Maze Runner");
        when(mockLibrary.returnBook("Maze Runner",user)).thenReturn("");
        System.setOut(mock(PrintStream.class));
        bibliotecaApp.start(mockInputHandler, mockOutput, mockLibrary,user,menu);
        verify(mockOutput, times(1)).show(menu.performOperation(4));
    }
}
