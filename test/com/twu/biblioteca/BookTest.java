package com.twu.biblioteca;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookTest {

    @Test
    public void equalsShouldReturnTrueIfTwoInstancesAreEquals() {
        Book book = new Book("Compiler Design", "Jeffrey Ullman", 1997);
        assertTrue(new Book("Compiler Design", "Jeffrey Ullman", 1997).equals(book));
    }

    @Test
    public void toStringShouldReturnBookDetails() {
        Book book = new Book("Compiler Design", "Jeffrey Ullman", 1977);
        String details = String.format("%-25s%-25s%-25d\n\n", "Compiler Design", "Jeffrey Ullman", 1977);
        assertTrue(book.toString().equals(details));
    }


    @Test
    public void checkNameShouldReturnFalseIfIGiveNameOfBookWhichIsInBookList() {
        Book book = new Book("Compiler Design", "Jeffrey Ullman", 1977);
        assertFalse(book.searchBook("Designing of Algorithms"));
    }

    @Test
    public void checkNameShouldReturnTrueIfIGiveNameOfBookWhichIsInBookList() {
        Book book = new Book("Compiler Design", "Jeffrey Ullman", 1977);
        assertTrue(book.searchBook("Compiler Design"));
    }
}
