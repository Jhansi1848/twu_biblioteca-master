package com.twu.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoginTest {

    @Test
    public void authenticateReturnTrueIfUserIsInList() throws IOException, InvalidDetailsException {
        Output output = mock(Output.class);
        BufferedReader bufferedReader = mock(BufferedReader.class);
        UserList userList = mock(UserList.class);
        User user = mock(User.class);
        when(bufferedReader.readLine()).thenReturn("user").thenReturn("password");
        when(userList.searchUser("user","password")).thenReturn(user);
        when(userList.size()).thenReturn(1);
        Login login = new Login(output,bufferedReader,userList);
        assertTrue(login.authenticate().equals(user));
    }
}
