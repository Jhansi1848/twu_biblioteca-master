package com.twu.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieParserTest {

    @Test
    public void parseLibraryShouldReturnLibraryObject() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("santhosham,2002,K.Dasarath,10").thenReturn(null);
        MovieParser movieParser = new MovieParser(bufferedReader);
        MovieList actualMovies = movieParser.parseMovie();
        assertEquals(1, actualMovies.size());
    }
}
