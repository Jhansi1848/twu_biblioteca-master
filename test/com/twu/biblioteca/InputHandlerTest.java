package com.twu.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class InputHandlerTest {

    @Test
    public void chooseOptionShouldReturnOption() throws IOException {
        Output output = mock(Output.class);
        BufferedReader bufferedReader = mock(BufferedReader.class);
        InputHandler inputHandler = new InputHandler(bufferedReader, output);
        when(bufferedReader.readLine()).thenReturn("1");
        System.setOut(mock(PrintStream.class));
        assertEquals(1, inputHandler.chooseOption());
    }
}
