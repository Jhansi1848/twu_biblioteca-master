package com.twu.biblioteca;

import org.junit.Test;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieListTest {
    @Test
    public void addShouldStoreAMovieObject() {
        Movie movie = mock(Movie.class);
        MovieList movieList = new MovieList();
        movieList.add(movie);
        assertEquals(1, movieList.size());
        assertTrue(movieList.contains(movie));
    }

    @Test
    public void removeShouldRemoveAMovieObjectFromList() {
        Movie movie = mock(Movie.class);
        MovieList movieList = new MovieList();
        movieList.add(movie);
        assertEquals(1, movieList.size());
        movieList.remove(movie);
        assertEquals(0, movieList.size());
        assertFalse(movieList.contains(movie));
    }

    @Test
    public void getShouldReturnAMovieObjectFromList() {
        Movie movie = mock(Movie.class);
        MovieList movieList = new MovieList();
        movieList.add(movie);
        assertEquals(1, movieList.size());
        assertTrue(movieList.get(0).equals(movie));
    }

    @Test
    public void searchMovieShouldReturnMovieIfAMovieIsThereInList() {
        Movie movie = mock(Movie.class);
        MovieList movieList = new MovieList();
        movieList.add(movie);
        when(movie.searchMovie("Data Mining")).thenReturn(true);
        assertEquals(1, movieList.size());
        assertTrue(movieList.searchMovie("Data Mining").equals(movie));
    }
}
