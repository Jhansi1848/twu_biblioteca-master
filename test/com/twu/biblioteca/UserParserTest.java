package com.twu.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserParserTest {
    @Test
    public void parseUserDetailsShouldReturnUserList() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("123-4567,mypassword,123@gmail.com").thenReturn(null);
        UserParser userParser = new UserParser(bufferedReader);
        UserList actualUsers = userParser.parseUserDetails();
        assertEquals(1, actualUsers.size());
    }
}
