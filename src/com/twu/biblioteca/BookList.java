package com.twu.biblioteca;

import java.util.ArrayList;

//Represents the list of book items
class BookList extends ArrayList {

    boolean add(Book book) {
        return super.add(book);
    }

    boolean remove(Book book) {
        return super.remove(book);
    }

    public int size() {
        return super.size();
    }

    boolean contains(Book book) {
        return super.contains(book);
    }

    public Book get(int index) {
        return (Book) super.get(index);
    }

    Book searchBook(String name) {
        for (int i = 0; i < size(); i++) {
            Book book = this.get(i);
            if (book.searchBook(name)) {
                return this.get(i);
            }
        }
        return new Book("", "", 0);
    }
}
