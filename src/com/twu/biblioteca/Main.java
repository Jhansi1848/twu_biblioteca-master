package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        FileReader bookFileReader = new FileReader("bookList");
        FileReader movieFileReader = new FileReader("moviesList");
        FileReader userFileReader = new FileReader("userList");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        BookParser bookParser = new BookParser(new BufferedReader(bookFileReader));
        MovieParser movieParser = new MovieParser(new BufferedReader(movieFileReader));
        UserParser userParser = new UserParser(new BufferedReader(userFileReader));

        BookList books = bookParser.parseBook();
        MovieList movies = movieParser.parseMovie();
        UserList users = userParser.parseUserDetails();

        Library library = new Library(books, movies);
        Output output = new Output();
        BibliotecaApp bibliotecaApp = new BibliotecaApp();
        Login login = new Login(output, bufferedReader, users);

        InputHandler inputHandler = new InputHandler(bufferedReader, output);
        try {
            User loggedInUser = null;
            while (loggedInUser == null) {
                loggedInUser = login.authenticate();
            }
            Menu menu = new Menu(library, new BufferedReader(new InputStreamReader(System.in)), loggedInUser);
            bibliotecaApp.start(inputHandler, output, library, loggedInUser, menu);
        } catch (IOException | NumberFormatException ex) {
            System.out.println(ex);
        }
    }
}
