package com.twu.biblioteca;

import java.util.ArrayList;

public class UserList extends ArrayList {

    public boolean add(User user) {
        return super.add(user);
    }

    public boolean remove(User user) {
        return super.remove(user);
    }

    public int size() {
        return super.size();
    }

    public boolean contains(User user) {
        return super.contains(user);
    }

    public User get(int index) {
        return (User) super.get(index);
    }

    public User searchUser(String username, String password) throws InvalidDetailsException {
        for (int i = 0; i < size(); i++) {
            if (this.get(i).searchUser(username, password)) {
                return this.get(i);
            }
        }
        throw new InvalidDetailsException("Enter valid details");
    }
}
