package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

public class MovieParser {

    private BufferedReader movieBufferedReader;

    MovieParser(BufferedReader movieBufferedReader) {
        this.movieBufferedReader = movieBufferedReader;
    }

    MovieList parseMovie() {
        MovieList movies = new MovieList();
        try {
            String line = movieBufferedReader.readLine();
            movies = createMovieList(line);
            movieBufferedReader.close();
        } catch (IOException ignored) {
        }
        return movies;
    }

    private MovieList createMovieList(String line) throws IOException {
        MovieList movies = new MovieList();
        while (line != null) {
            String[] details = line.split(",");
            movies.add(getMovie(details));
            line = movieBufferedReader.readLine();
        }
        return movies;
    }

    private Movie getMovie(String[] details) {
        String name = details[0];
        String year = details[1];
        String director = details[2];
        String rating = details[3];
        return new Movie(name, Integer.parseInt(year), director, Double.parseDouble(rating));
    }
}
