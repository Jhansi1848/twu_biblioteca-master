package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

public class UserParser {

    private BufferedReader userBufferedReader;

    public UserParser(BufferedReader userBufferedReader) {
        this.userBufferedReader = userBufferedReader;
    }

    UserList parseUserDetails() {
        UserList users = new UserList();
        try {
            String line = userBufferedReader.readLine();
            users = createUserList(userBufferedReader, line);
            userBufferedReader.close();
        } catch (IOException ignored) {
        }
        return users;
    }

    private UserList createUserList(BufferedReader userBufferedReader, String line) throws IOException {
        UserList users = new UserList();
        while (line != null) {
            String[] details = line.split(",");
            users.add(getUserDetails(details));
            line = userBufferedReader.readLine();
        }
        return users;
    }

    private User getUserDetails(String[] details) {
        String username = details[0];
        String password = details[1];
        String email = details[2];
        return new User(username, password, email);
    }
}
