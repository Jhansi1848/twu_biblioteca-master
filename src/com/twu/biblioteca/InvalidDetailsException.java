package com.twu.biblioteca;

public class InvalidDetailsException extends Throwable {
    public InvalidDetailsException(String message) {
        super(message);
    }
}
