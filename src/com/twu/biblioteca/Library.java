package com.twu.biblioteca;

import java.util.HashMap;

//understands collections of books
public class Library {

    public static final String INVALID_ACTIVITY = "Enter book name";
    static final String SUCCESSFUL_CHECKOUT = "Thank you! Enjoy the book";
    static final String UNSUCCESSFUL_CHECKOUT = "That book is not available";
    static final String UNSUCCESSFUL_CHECKIN = "That is not a valid book to return";
    static final String SUCCESSFUL_CHECKIN = "Thank you for returning the book";
    public static final String EMPTY_LIBRARY = "sorry, No books are available";
    private static final String SUCCESSFUL_CHECKOUT_MOVIE = "Thank you! Enjoy the movie";
    private static final String UNSUCCESSFUL_CHECKOUT_MOVIE = "That movie is not available";

    private BookList books;
    private BookList checkedOutBookList = new BookList();
    private MovieList checkedOutMovieList = new MovieList();
    private MovieList movies;
    private HashMap<Book, User> bookRegister = new HashMap<>();
    private HashMap<Movie, User> movieRegister = new HashMap<>();

    Library(BookList books, MovieList movies) {
        this.books = books;
        this.movies = movies;
    }

    public String displayBookList() {
        String bookList = "";
        if (books.isEmpty())
            return EMPTY_LIBRARY;
        for (Object book : books) {
            bookList = bookList + book.toString();
        }
        return bookList;
    }

    public String displayMovieList() {
        String movieList = "";
        if (movies.isEmpty())
            return EMPTY_LIBRARY;
        for (Object movie : movies) {
            movieList = movieList + movie.toString();
        }
        return movieList;
    }

    String welcomeMessage() {
        return "Hello, Welcome to the Library!!!";
    }

    public String checkout(String bookName, User user) {
        boolean status = false;
        if (books.searchBook(bookName) != null) {
            checkedOutBookList.add(books.searchBook(bookName));
            bookRegister.put(books.searchBook(bookName), user);
            status = books.remove(books.searchBook(bookName));

        }
        if (status) {
            return SUCCESSFUL_CHECKOUT;
        }
        return UNSUCCESSFUL_CHECKOUT;
    }

    public String checkoutMovie(String movieName, User user) {
        boolean status = false;
        if (movies.searchMovie(movieName) != null) {
            checkedOutMovieList.add(movies.searchMovie(movieName));
            movieRegister.put(movies.searchMovie(movieName), user);
            status = movies.remove(movies.searchMovie(movieName));
        }
        if (status) {
            return SUCCESSFUL_CHECKOUT_MOVIE;
        }
        return UNSUCCESSFUL_CHECKOUT_MOVIE;
    }

    public String returnBook(String returnBookName, User user) {
        boolean status = false;
        System.out.println(returnBookName);
        Book book = checkedOutBookList.searchBook(returnBookName);
        User checkedOut = bookRegister.get(book);
        if (checkedOut == null) {
            return UNSUCCESSFUL_CHECKIN;
        }

        if (checkedOut.equals(user)) {
            status = books.add(checkedOutBookList.searchBook(returnBookName));
            bookRegister.remove(checkedOutBookList.searchBook(returnBookName));
        }

        if (status) {
            return SUCCESSFUL_CHECKIN;
        }
        return UNSUCCESSFUL_CHECKIN;
    }
}
