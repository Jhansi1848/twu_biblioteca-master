package com.twu.biblioteca;

import java.io.IOException;

interface Input {
    int chooseOption() throws IOException;
}
