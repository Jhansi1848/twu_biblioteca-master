package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

//understands the list of books in file
class BookParser {

    private BufferedReader bookBufferedReader;

    BookParser(BufferedReader bookBufferedReader) {
        this.bookBufferedReader = bookBufferedReader;
    }

    static private Book getBook(String[] details) {
        String book = details[0];
        String author = details[1];
        String year = details[2];
        return new Book(book, author, Integer.parseInt(year));
    }

    BookList parseBook() {
        BookList bookList = new BookList();
        try {
            String line = bookBufferedReader.readLine();
            bookList = createBookList(bookBufferedReader, line);
            bookBufferedReader.close();
        } catch (IOException ignored) {
        }
        return bookList;
    }

    private BookList createBookList(BufferedReader br, String line) throws IOException {
        BookList bookList = new BookList();
        while (line != null) {
            String[] details = line.split(",");
            bookList.add(getBook(details));
            line = br.readLine();
        }
        return bookList;
    }
}
