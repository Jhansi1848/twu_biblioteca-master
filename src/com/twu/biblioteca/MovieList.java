package com.twu.biblioteca;

import java.util.ArrayList;

class MovieList extends ArrayList {
    boolean add(Movie movie) {
        return super.add(movie);
    }

    public boolean remove(Movie movie) {
        return super.remove(movie);
    }

    public int size() {
        return super.size();
    }

    public boolean contains(Movie movie) {
        return super.contains(movie);
    }

    public Movie get(int index) {
        return (Movie) super.get(index);
    }

    public Movie searchMovie(String name) {
        for (int i = 0; i < size(); i++) {
            if (this.get(i).searchMovie(name)) {

                return this.get(i);
            }
        }
        return null;
    }
}
