package com.twu.biblioteca;


import java.io.IOException;

//Understands how to manage library management system
class BibliotecaApp {

    void start(InputHandler inputHandler, Output output, Library library, User loggedInUser, Menu menu) throws IOException {
        output.show(library.welcomeMessage() + "\n");
        output.show(loggedInUser.toString());
        int option = inputHandler.chooseOption();

        while (option != 6) {
            output.show(menu.performOperation(option));
            output.show(loggedInUser.toString());
            option = inputHandler.chooseOption();
        }
        System.exit(0);
    }
}
