package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

class Login {

    private Output output;
    private BufferedReader bufferedReader;
    private UserList userList;

    Login(Output output, BufferedReader bufferedReader, UserList userList) {
        this.output = output;
        this.bufferedReader = bufferedReader;
        this.userList = userList;

    }

    User authenticate() throws IOException {
        boolean status = false;
        output.show("Enter username");
        String username = bufferedReader.readLine();
        output.show("Enter password");
        String password = bufferedReader.readLine();
        try {
            return userList.searchUser(username, password);
        } catch (InvalidDetailsException ex) {
            System.out.println(ex);
        }
        return null;
    }
}
