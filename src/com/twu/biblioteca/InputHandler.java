package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

//Understands the handling of user choice
class InputHandler implements Input {

    private BufferedReader bufferedReader;
    private Output output;

    InputHandler(BufferedReader bufferedReader, Output output) {

        this.bufferedReader = bufferedReader;
        this.output = output;
    }

    public int chooseOption() throws IOException {
        output.showMenu();
        int option;
        try {
            option = Integer.parseInt(bufferedReader.readLine());
            return option;
        } catch (java.io.IOException | NumberFormatException ignored) {
        }
        return -1;
    }
}
