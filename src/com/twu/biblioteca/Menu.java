package com.twu.biblioteca;

import commands.*;

import java.io.BufferedReader;

public class Menu {

    private Library library;
    private BufferedReader bufferedReader;
    private User user;
    private Command command;

    Menu(Library library, BufferedReader bufferedReader, User user) {
        this.library = library;
        this.bufferedReader = bufferedReader;
        this.user = user;
    }

    public String performOperation(int option) throws java.io.IOException {
        switch (option) {
            case 1:
                command = new DisplayListCommand(library);
                return command.execute();
            case 2:
                command = new CheckOutBookCommand(library, bufferedReader, user);
                return command.execute();
            case 3:
                command = new ReturnBookCommand(library, bufferedReader, user);
                return command.execute();
            case 4:
                command = new MovieListCommand(library);
                return command.execute();
            case 5:
                command = new CheckOutMovieCommand(library, bufferedReader, user);
                return command.execute();
            default:
                return "Select a valid option!";
        }
    }
}
