package com.twu.biblioteca;

public class Movie {
    private String name;
    private int year;
    private String director;
    private double rating;

    Movie(String name, int year, String director, double rating) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return String.format("%-25s%-25d%-25s%-25f\n\n", this.name, this.year, this.director, this.rating);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (year != movie.year) return false;
        if (Double.compare(movie.rating, rating) != 0) return false;
        if (name != null ? !name.equals(movie.name) : movie.name != null) return false;
        return director != null ? director.equals(movie.director) : movie.director == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        result = 31 * result + year;
        result = 31 * result + (director != null ? director.hashCode() : 0);
        temp = Double.doubleToLongBits(rating);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public boolean searchMovie(String movieName) {
        return movieName.equalsIgnoreCase(name);
    }
}
