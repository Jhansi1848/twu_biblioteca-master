package commands;

import com.twu.biblioteca.Library;
import com.twu.biblioteca.User;

import java.io.BufferedReader;
import java.io.IOException;

public class ReturnBookCommand implements Command {

    private Library library;
    private BufferedReader bufferedReader;
    private User user;

    public ReturnBookCommand(Library library, BufferedReader bufferedReader, User user) {
        this.library = library;
        this.bufferedReader = bufferedReader;
        this.user = user;
    }

    @Override
    public String execute() throws IOException {
        String returnBookName = bufferedReader.readLine().trim();
        return library.returnBook(returnBookName, user);
    }
}
