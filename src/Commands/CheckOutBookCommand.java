package commands;

import com.twu.biblioteca.Library;
import com.twu.biblioteca.User;

import java.io.BufferedReader;
import java.io.IOException;

public class CheckOutBookCommand implements Command {

    private Library library;
    private BufferedReader bufferedReader;
    private User user;

    public CheckOutBookCommand(Library library, BufferedReader bufferedReader, User user) {
        this.library = library;
        this.bufferedReader = bufferedReader;
        this.user = user;
    }

    @Override
    public String execute() throws IOException {
        String newBookName = bufferedReader.readLine();
        return library.checkout(newBookName, user);
    }
}
