package commands;

import com.twu.biblioteca.Library;

import static com.twu.biblioteca.Library.EMPTY_LIBRARY;

public class DisplayListCommand implements Command {

    private Library library;

    public DisplayListCommand(Library library) {
        this.library = library;
    }

    @Override
    public String execute() {
        String bookList = "";
        String books = library.displayBookList();
        if (books.equals(EMPTY_LIBRARY))
            return EMPTY_LIBRARY;
        bookList = String.format("%-25s%-25s%-25s\n\n", "book", "Author", "year");
        bookList = bookList + books;
        return bookList;
    }
}
