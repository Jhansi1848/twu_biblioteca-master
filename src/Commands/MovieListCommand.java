package commands;

import com.twu.biblioteca.Library;

import static com.twu.biblioteca.Library.EMPTY_LIBRARY;

public class MovieListCommand implements Command {

    private Library library;

    public MovieListCommand(Library library) {
        this.library = library;
    }

    @Override
    public String execute() {
        String movieList = "";
        String movies = library.displayMovieList();
        if (movies.equals(EMPTY_LIBRARY))
            return EMPTY_LIBRARY;
        movieList = String.format("%-25s%-25s%-25s%-25s\n\n", "movie", "year", "director", "rating");
        movieList = movieList + movies;
        return movieList;
    }
}